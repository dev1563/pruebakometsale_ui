import { environment } from "src/environments/environment";

export const endpoints = {
    saveFlowers: environment.routeApiPrueba + '/saveFlower',
    findAll: environment.routeApiPrueba + '/listSaved',
    findPrice: environment.routeApiPrueba + '/listPrice'
}
