import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PruebaKometSalesComponent } from './components/prueba-komet-sales/prueba-komet-sales.component';
import { FormsModule } from '@angular/forms';
import { PruebaService } from './service/prueba.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    PruebaKometSalesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    PruebaService  
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
