import { Component, OnInit } from '@angular/core';
import { PruebaService } from 'src/app/service/prueba.service';

@Component({
  selector: 'app-prueba-komet-sales',
  templateUrl: './prueba-komet-sales.component.html',
  styleUrls: ['./prueba-komet-sales.component.css']
})
export class PruebaKometSalesComponent implements OnInit {

  public valueName: string = '';
  public valuePrice: number = 0;
  public listFlowers: any = [];
  public isList: boolean = false;
  public listFlowersPrice: any = [];
  public isListPrice: boolean = false;

  constructor(private service: PruebaService) { }

  ngOnInit(): void {
  }

  
  saveData(name: string, price: number){
    let auxFlower = {
      "id": 0,
      "name" : name,
      "price" : price
    }
    this.service.saveFlower(auxFlower);
  }

  findAll(){
    this.service.findAll().subscribe(
      data => {
        this.listFlowers = data.response;
        this.isList = true;
      })
  }

  findPrice(){
    this.service.findPrice().subscribe(
      data => {
        this.listFlowersPrice = data.response;
        this.isListPrice = true;
      })
  }

  deleteFlower(id: number){
    
  }

}
