import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PruebaKometSalesComponent } from './prueba-komet-sales.component';

describe('PruebaKometSalesComponent', () => {
  let component: PruebaKometSalesComponent;
  let fixture: ComponentFixture<PruebaKometSalesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PruebaKometSalesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PruebaKometSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
