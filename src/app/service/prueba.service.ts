import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Flower } from '../model/flower.model';
import { endpoints } from '../utils/endpoints';
import { Observable, catchError, from } from 'rxjs';

@Injectable()
export class PruebaService {

  constructor(private http: HttpClient) { }

  saveFlower(Flower: Flower ){
    let url = endpoints.saveFlowers;
    return this.http.post(url, Flower).toPromise()
    .then((data) => data)
    .catch((data) => data);
  }

  findAll(): Observable<any> {
    let url = endpoints.findAll;
    return from(this.http.get(url).toPromise())
  }
  
  findPrice(): Observable<any>{
    let url = endpoints.findPrice;
    return from(this.http.get(url).toPromise())
  }


}

