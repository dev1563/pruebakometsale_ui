export interface Flower {
    id:            number;
    name:   string;
    price: number;
}